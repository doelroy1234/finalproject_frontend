import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { QuizService } from 'src/app/services/quiz.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-update-quiz',
  templateUrl: './update-quiz.component.html',
  styleUrls: ['./update-quiz.component.css']
})
export class UpdateQuizComponent implements OnInit {
constructor(private _route: ActivatedRoute,
  private _quiz: QuizService,
  private _cat: CategoryService,
  private _router: Router,
  private _snack: MatSnackBar){}
  qId: any =0 ;
  quiz: any;
  categories: any;
ngOnInit(): void{
  this.qId = this._route.snapshot.params['qid'];
 // this._snack.open(this.qId,'ok');
  this._quiz.getQuiz(this.qId).subscribe(
    (data: any) => {
      this.quiz = data;
      console.log(this.quiz);
    },
    (error) => {
      console.log(error);
    }
  );

  this._cat.categories().subscribe(
    (data: any) => {
      this.categories = data;
    },
    (error) => {
      Swal.fire('Error !!!','Error in loading quizzes', 'error');
    }
  );
}

//update form submit
public updateData() {
  //validatate

  this._quiz.updateQuiz(this.quiz).subscribe(
    (data) => {
      Swal.fire('Success !!!', 'Quiz Updated', 'success').then((e) => {
        this._router.navigate(['/admin/quizzes']);
      });
    },
    (error) => {
      Swal.fire('Error !!!', 'Error in updating Quiz', 'error');
      console.log(error);
    }
  );
}
}
