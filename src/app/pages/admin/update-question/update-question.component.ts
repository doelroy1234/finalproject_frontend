import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';
import { QuizService } from 'src/app/services/quiz.service';
import Swal from 'sweetalert2';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-update-question',
  templateUrl: './update-question.component.html',
  styleUrls: ['./update-question.component.css']
})
export class UpdateQuestionComponent implements OnInit{
  constructor(private _route: ActivatedRoute,
    private _question: QuestionService,
    private _quiz: QuizService,
    private _router: Router,
    private _snack: MatSnackBar){}

    public Editor : any = ClassicEditor;
    qId: any;
    qTitle: any;
    questions = [] as any;
    question = {
      quiz: {},
      content: '',
      option1: '',
      option2: '',
      option3: '',
      option4: '',
      answer: '',
    };
    ngOnInit(): void{
      this.qId = this._route.snapshot.params['qid'];
     // this._snack.open(this.qId,'ok');
      this._question.getQuestion(this.qId).subscribe(
        (data: any) => {
          console.log(data);
          this.question = data;
        },
        (error) => {
          console.log(error);
        }
      );
    
      this._quiz.quizzes().subscribe(
        (data: any) => {
          this.questions = data;
        },
        (error) => {
          Swal.fire('Error !!!','Error in loading questions', 'error');
        }
      );
    }
    
    //update form submit
    public updateData() {
      //validatate
    
      this._question.updateQuestion(this.question).subscribe(
        (data) => {
          Swal.fire('Success !!!', 'Question Updated', 'success').then((e) => {
            this._router.navigate(['/admin/quizzes']);
          });
        },
        (error) => {
          Swal.fire('Error !!!', 'Error in updating Question', 'error');
          console.log(error);
        }
      );
    }
}

