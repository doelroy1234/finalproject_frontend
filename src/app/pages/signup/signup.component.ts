import { Component, OnInit} from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit{
constructor(private userService: UserService, private snack: MatSnackBar, private toastr: ToastrService){}

public user={
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    emailId: '',
    phone: '',
  }

ngOnInit(): void {}

formSubmit(){
  console.log(this.user);
  if(this.user.username=='' || this.user.username == null){
        this.toastr.error('Username required :( !!!');
    //this.snack.open('Username required :( !!!', 'ok',{verticalPosition: 'bottom'});
    return;
  }
  if (this.user.password == '' || this.user.password == null) {
    this.toastr.error('Password required :( !!!');
    //this.snack.open('Password is required !!!', 'ok',{verticalPosition: 'bottom'});
    return;
  }
  // if (this.user.emailId.indexOf("@") === -1 && this.user.emailId.indexOf(".com") === -1) {
  //   this.snack.open('Invalid email address !!!', 'ok');
  // } 
  

  this.userService.addUser(this.user).subscribe(
    (data: any) => {
      console.log(data);
      Swal.fire('Registration Successfull ;)','User ID : '+data.id, 'success');
    },
    (error) => {
      console.log(error);
      this.snack.open('OOPS! Something went wrong :(', 'ok',{verticalPosition: 'bottom'});

    }
  )
}

}
